﻿namespace NiRecorder.Net
{
    partial class frm_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cb_devices = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_submit = new System.Windows.Forms.Button();
            this.pb_image = new System.Windows.Forms.PictureBox();
            this.cbInfrared = new System.Windows.Forms.CheckBox();
            this.cbDepth = new System.Windows.Forms.CheckBox();
            this.cbColor = new System.Windows.Forms.CheckBox();
            this.edFilePath = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.cbPreviewSensor = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pb_image)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 371);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Device:";
            // 
            // cb_devices
            // 
            this.cb_devices.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_devices.FormattingEnabled = true;
            this.cb_devices.Location = new System.Drawing.Point(76, 368);
            this.cb_devices.Name = "cb_devices";
            this.cb_devices.Size = new System.Drawing.Size(142, 21);
            this.cb_devices.TabIndex = 1;
            this.cb_devices.SelectedIndexChanged += new System.EventHandler(this.CbDevicesSelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(255, 371);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Sensors:";
            // 
            // btn_submit
            // 
            this.btn_submit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_submit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_submit.Location = new System.Drawing.Point(322, 450);
            this.btn_submit.Name = "btn_submit";
            this.btn_submit.Size = new System.Drawing.Size(98, 35);
            this.btn_submit.TabIndex = 12;
            this.btn_submit.Text = "Record";
            this.btn_submit.UseVisualStyleBackColor = true;
            this.btn_submit.Click += new System.EventHandler(this.BtnSubmitClick);
            // 
            // pb_image
            // 
            this.pb_image.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pb_image.BackColor = System.Drawing.Color.Black;
            this.pb_image.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pb_image.ErrorImage = null;
            this.pb_image.InitialImage = null;
            this.pb_image.Location = new System.Drawing.Point(12, 12);
            this.pb_image.Name = "pb_image";
            this.pb_image.Size = new System.Drawing.Size(726, 339);
            this.pb_image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_image.TabIndex = 15;
            this.pb_image.TabStop = false;
            // 
            // cbInfrared
            // 
            this.cbInfrared.AutoSize = true;
            this.cbInfrared.Location = new System.Drawing.Point(310, 370);
            this.cbInfrared.Name = "cbInfrared";
            this.cbInfrared.Size = new System.Drawing.Size(62, 17);
            this.cbInfrared.TabIndex = 16;
            this.cbInfrared.Text = "Infrared";
            this.cbInfrared.UseVisualStyleBackColor = true;
            // 
            // cbDepth
            // 
            this.cbDepth.AutoSize = true;
            this.cbDepth.Location = new System.Drawing.Point(378, 370);
            this.cbDepth.Name = "cbDepth";
            this.cbDepth.Size = new System.Drawing.Size(55, 17);
            this.cbDepth.TabIndex = 17;
            this.cbDepth.Text = "Depth";
            this.cbDepth.UseVisualStyleBackColor = true;
            // 
            // cbColor
            // 
            this.cbColor.AutoSize = true;
            this.cbColor.Location = new System.Drawing.Point(439, 370);
            this.cbColor.Name = "cbColor";
            this.cbColor.Size = new System.Drawing.Size(50, 17);
            this.cbColor.TabIndex = 18;
            this.cbColor.Text = "Color";
            this.cbColor.UseVisualStyleBackColor = true;
            // 
            // edFilePath
            // 
            this.edFilePath.Location = new System.Drawing.Point(12, 413);
            this.edFilePath.Name = "edFilePath";
            this.edFilePath.Size = new System.Drawing.Size(396, 20);
            this.edFilePath.TabIndex = 19;
            this.edFilePath.TextChanged += new System.EventHandler(this.edFilePath_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 397);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "File to record:";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(414, 411);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 21;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(531, 371);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Preview:";
            // 
            // cbPreviewSensor
            // 
            this.cbPreviewSensor.FormattingEnabled = true;
            this.cbPreviewSensor.Location = new System.Drawing.Point(586, 370);
            this.cbPreviewSensor.Name = "cbPreviewSensor";
            this.cbPreviewSensor.Size = new System.Drawing.Size(121, 21);
            this.cbPreviewSensor.TabIndex = 23;
            // 
            // frm_Main
            // 
            this.AcceptButton = this.btn_submit;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 497);
            this.Controls.Add(this.cbPreviewSensor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.edFilePath);
            this.Controls.Add(this.cbColor);
            this.Controls.Add(this.cbDepth);
            this.Controls.Add(this.cbInfrared);
            this.Controls.Add(this.pb_image);
            this.Controls.Add(this.btn_submit);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cb_devices);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(580, 300);
            this.Name = "frm_Main";
            this.Text = "NiRecorder.Net";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMainFormClosing);
            this.Load += new System.EventHandler(this.FrmMainLoad);
            ((System.ComponentModel.ISupportInitialize)(this.pb_image)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cb_devices;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_submit;
        private System.Windows.Forms.PictureBox pb_image;
        private System.Windows.Forms.CheckBox cbInfrared;
        private System.Windows.Forms.CheckBox cbDepth;
        private System.Windows.Forms.CheckBox cbColor;
        private System.Windows.Forms.TextBox edFilePath;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbPreviewSensor;
    }
}

