# NiRecorder .Net #

This app aims to help record data from OpenNi 2 compatible devices. The main device used for tests was Kinect 1.8. The app allows to select device for the recording, specify the output file (.oni) and select the desired streams.

![screen.jpg](https://bitbucket.org/repo/6gGKaG/images/1803340776-screen.jpg)

### How do I get set up? ###

In order to build the solution you will need [NiWrapper.Net](https://github.com/falahati/NiWrapper.Net). Get it first!

* Open the solution in Visual Studio and build it.
* To actually run the app the following should be in the same folder as your exe:
    * OpenNI2 folder with drivers
    * NiWrapper.dll
    * OpenNI2.dll
    
### Download ###

There is a binary package in [Downloads](https://bitbucket.org/cnc-ntnu/nirecorder/downloads/) section.

### License ###

Copyright (C) 2014 Vadim Frolov - vadim.frolov@ntnu.no

This program is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2.1 of the License.txt, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.