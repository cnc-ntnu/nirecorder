﻿namespace NiRecorder.Net
{
    #region

    using System;
    using System.IO;
    using System.Diagnostics.CodeAnalysis;
    using System.Drawing;
    using System.Windows.Forms;

    using OpenNIWrapper;

    #endregion

    [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Reviewed. Suppression is OK here.")]
    // ReSharper disable once InconsistentNaming
    public partial class frm_Main : Form
    {
        #region Fields

        private Bitmap _bitmap;

        private Device _currentDevice;

        //private VideoStream currentSensor;
        private VideoStream _irStream;
        private VideoStream _depthStream;
        private VideoStream _colorStream;

        private String _lastDir;
        private String _oniName;

        private bool _isRecording;
        private Recorder _recorder;

        #endregion

        #region Constructors and Destructors

        public frm_Main()
        {
            InitializeComponent();
            _bitmap = new Bitmap(1, 1);
            _oniName = "./captured.oni";

            edFilePath.TextChanged -= edFilePath_TextChanged;
            edFilePath.Text = _oniName;
            edFilePath.TextChanged += edFilePath_TextChanged;

            _isRecording = false;
        }

        #endregion

        #region Methods

        protected override void OnPaint(PaintEventArgs e)
        {
            if (pb_image.Visible)
            {
                pb_image.Visible = false;
            }

            if (_bitmap == null)
            {
                return;
            }

            lock (_bitmap)
            {
                // OnPaint happens on UI Thread so it is better to always keep this lock in place
                var canvasSize = pb_image.Size; // Even though we dont use PictureBox, we use it as a placeholder
                var canvasPosition = pb_image.Location;

                var ratioX = canvasSize.Width / (double)_bitmap.Width;
                var ratioY = canvasSize.Height / (double)_bitmap.Height;
                var ratio = Math.Min(ratioX, ratioY);

                var drawWidth = Convert.ToInt32(_bitmap.Width * ratio);
                var drawHeight = Convert.ToInt32(_bitmap.Height * ratio);

                var drawX = canvasPosition.X + Convert.ToInt32((canvasSize.Width - drawWidth) / 2);
                var drawY = canvasPosition.Y + Convert.ToInt32((canvasSize.Height - drawHeight) / 2);

                e.Graphics.DrawImage(_bitmap, drawX, drawY, drawWidth, drawHeight);

                /////////////////////// If we do create a new Bitmap object per each frame we must
                /////////////////////// make sure to DISPOSE it after using.
                // bitmap.Dispose();
                /////////////////////// END NOTE
            }
        }

        public static void HandleError(OpenNI.Status status)
        {
            if (status == OpenNI.Status.Ok)
            {
                return;
            }

            MessageBox.Show(
                string.Format(@"Error: {0} - {1}", status, OpenNI.LastError), 
                @"Error", 
                MessageBoxButtons.OK, 
                MessageBoxIcon.Asterisk);
        }

        private void OpenNiOnDeviceConnectionStateChanged(DeviceInfo device)
        {
            BeginInvoke(new MethodInvoker(UpdateDevicesList));
        }

        private void UpdateDevicesList()
        {
            if (_isRecording)
            {
                StopRecording();
            }
            var devices = OpenNI.EnumerateDevices();
            cb_devices.Items.Clear();
            foreach (var device in devices)
            {
                cb_devices.Items.Add(device);
            }

            if (cb_devices.Items.Count > 0)
                cb_devices.SelectedIndex = 0;
        }

        private void StopRecording()
        {
            if (!_recorder.IsValid)
            {
                return;
            }

            if (cbColor.Checked)
            {
                _colorStream.Stop();
            }
            if (cbInfrared.Checked)
            {
                _irStream.Stop();
            }
            if (cbDepth.Checked)
            {
                _depthStream.Stop();
            }

            // detach preview callback
            if (cbPreviewSensor.SelectedItem.ToString() == "Color")
            {
                _colorStream.OnNewFrame -= CurrentSensorOnNewFrame;
            }
            if (cbPreviewSensor.SelectedItem.ToString() == "Infrared")
            {
                _irStream.OnNewFrame -= CurrentSensorOnNewFrame;
            }
            if (cbPreviewSensor.SelectedItem.ToString() == "Depth")
            {
                _depthStream.OnNewFrame -= CurrentSensorOnNewFrame;
            }

            _recorder.Stop();
            _isRecording = false;
            _recorder.Destroy();
            btn_submit.Text = @"Record";
            EnableControls(true);
        }

        private void BtnSubmitClick(object sender, EventArgs e)
        {
            if (!_currentDevice.IsValid)
                return;

            if (!_colorStream.IsValid)
                return;

            if (_isRecording)
            {
                StopRecording();
            }
            else
            {
                if (cbColor.Checked && cbDepth.Checked)
                    _currentDevice.DepthColorSyncEnabled = true;

                const String oniExt = @".oni";

                if (!_oniName.EndsWith(oniExt))
                {
                    _oniName += oniExt;
                    edFilePath.TextChanged -= edFilePath_TextChanged;
                    edFilePath.Text = _oniName;
                    edFilePath.TextChanged += edFilePath_TextChanged;
                }

                _recorder = Recorder.Create(_oniName);
                if (cbColor.Checked)
                {
                    _recorder.Attach(_colorStream, true);
                    if (cbPreviewSensor.SelectedItem.ToString() == "Color")
                    {
                        _colorStream.OnNewFrame += CurrentSensorOnNewFrame;
                    }

                    _colorStream.Start();
                }
                if (cbDepth.Checked)
                {
                    _recorder.Attach(_depthStream, true);
                    if (cbPreviewSensor.SelectedItem.ToString() == "Depth")
                    {
                        _depthStream.OnNewFrame += CurrentSensorOnNewFrame;
                    }
                    _depthStream.Start();
                }
                if (cbInfrared.Checked)
                {
                    _recorder.Attach(_irStream, true);
                    if (cbPreviewSensor.SelectedItem.ToString() == "Infrared")
                    {
                        _irStream.OnNewFrame += CurrentSensorOnNewFrame;
                    }
                    _irStream.Start();
                }

                _recorder.Start();
                btn_submit.Text = @"Stop";
                _isRecording = true;
                EnableControls(false);
            }
        }

        private void EnableControls(bool enable)
        {
            cbColor.Enabled = enable;
            cbDepth.Enabled = enable;
            cbInfrared.Enabled = enable;
            btnBrowse.Enabled = enable;
            edFilePath.Enabled = enable;
            cb_devices.Enabled = enable;
            cbPreviewSensor.Enabled = enable;
        }

        private void CbDevicesSelectedIndexChanged(object sender, EventArgs e)
        {
            if (cb_devices.SelectedItem != null)
            {
                if (_currentDevice != null)
                {
                    _currentDevice.Dispose();
                }
                
                cbInfrared.Checked = false;
                cbDepth.Checked = false;
                cbColor.Checked = false;
                var defaultPreview = 0;

                _currentDevice = ((DeviceInfo)cb_devices.SelectedItem).OpenDevice();
                if (_currentDevice.HasSensor(Device.SensorType.Color))
                {
                    //cb_sensor.Items.Add("Color");
                    cbColor.Checked = true;
                    cbPreviewSensor.Items.Add("Color");
                }

                if (_currentDevice.HasSensor(Device.SensorType.Depth))
                {
                    cbDepth.Checked = true;
                    cbPreviewSensor.Items.Add("Depth");
                }

                if (_currentDevice.HasSensor(Device.SensorType.Ir))
                {
                    // Do not enable it by default
                    //cbInfrared.Checked = true;
                    defaultPreview = cbPreviewSensor.Items.Add("Infrared");
                }
                cbPreviewSensor.SelectedIndex = defaultPreview;

                try
                {
                    // Not supported by Kinect yet
                    if (_currentDevice.IsImageRegistrationModeSupported(Device.ImageRegistrationMode.DepthToColor))
                    {
                        _currentDevice.ImageRegistration = Device.ImageRegistrationMode.DepthToColor;
                    }
                    else
                    {
                        _currentDevice.ImageRegistration = Device.ImageRegistrationMode.Off;
                    }
                }
                catch (Exception)
                {
                }

                _irStream = _currentDevice.CreateVideoStream(Device.SensorType.Ir);
                _depthStream = _currentDevice.CreateVideoStream(Device.SensorType.Depth);
                _colorStream = _currentDevice.CreateVideoStream(Device.SensorType.Color);
            }
        }

        private void CurrentSensorOnNewFrame(VideoStream videoStream)
        {
            if (videoStream.IsValid && videoStream.IsFrameAvailable())
            {
                using (VideoFrameRef frame = videoStream.ReadFrame())
                {
                    if (frame.IsValid)
                    {
                        //MessageBox.Show("Selected item ");

                        const VideoFrameRef.CopyBitmapOptions options = VideoFrameRef.CopyBitmapOptions.Force24BitRgb
                                                                  | VideoFrameRef.CopyBitmapOptions.DepthFillShadow;
                        /*if (cb_invert.Checked)
                        {
                            options |= VideoFrameRef.CopyBitmapOptions.DepthInvert;
                        }

                        if (cb_equal.Checked)
                        {
                            options |= VideoFrameRef.CopyBitmapOptions.DepthHistogramEqualize;
                        }

                        if (cb_fill.Checked)
                        {
                            options |= videoStream.Mirroring
                                           ? VideoFrameRef.CopyBitmapOptions.DepthFillRigthBlack
                                           : VideoFrameRef.CopyBitmapOptions.DepthFillLeftBlack;
                        }*/

                        lock (_bitmap)
                        {
                            /////////////////////// Instead of creating a bitmap object for each frame, you can simply
                            /////////////////////// update one you have. Please note that you must be very careful 
                            /////////////////////// with multi-thread situations.
                            try
                            {
                                frame.UpdateBitmap(_bitmap, options);
                            }
                            catch (Exception)
                            {
                                // Happens when our Bitmap object is not compatible with returned Frame
                                _bitmap = frame.ToBitmap(options);
                            }

                            /////////////////////// END NOTE

                            /////////////////////// You can always use .toBitmap() if you dont want to
                            /////////////////////// clone image later and be safe when using it in multi-thread situations
                            /////////////////////// This is little slower, but easier to handle
                            // bitmap = frame.toBitmap(options);
                            /////////////////////// END NOTE
                            /*if (cb_mirrorSoft.Checked)
                            {
                                bitmap.RotateFlip(RotateFlipType.RotateNoneFlipX);
                            }*/
                        }

                        ///////////////////// You can simply pass the newly created/updated image to a
                        ///////////////////// PictureBox right here instead of drawing it with Graphic object
                        // BeginInvoke(new MethodInvoker(delegate()
                        // {
                        // if (!pb_image.Visible)
                        // pb_image.Visible = true;
                        // if (bitmap == null)
                        // return;
                        // lock (bitmap) // BeginInvoke happens on UI Thread so it is better to always keep this lock in place
                        // {
                        // if (pb_image.Image != null)
                        // pb_image.Image.Dispose();

                        // /////////////////////// If you want to use one bitmap object for all frames, the 
                        // /////////////////////// best way to prevent and multi-thread access problems
                        // /////////////////////// is to clone the bitmap each time you want to send it to PictureBox 
                        // pb_image.Image = new Bitmap(bitmap, bitmap.Size);
                        // /////////////////////// END NOTE

                        // /////////////////////// If you only use toBitmap() method. you can simply skip the
                        // /////////////////////// cloning process. It is perfectly thread-safe.
                        // //pb_image.Image = bitmap;
                        // /////////////////////// END NOTE

                        // pb_image.Refresh();
                        // }
                        // }));
                        ///////////////////// END NOTE
                        if (!pb_image.Visible)
                        {
                            Invalidate();
                        }
                    }
                }
            }
        }

        /////////////////////// You can use one Bitmap object and update it for each frame 
        /////////////////////// or one Bitmap object per each frame. Either way you can use
        /////////////////////// OnPaint method to prevent multi-thread access problems
        /////////////////////// without creating a new Bitmap object each time.
        /////////////////////// In other word, you can skip Cloning even when using updateBitmap().

        /////////////////////// END NOTE
        private void FrmMainFormClosing(object sender, FormClosingEventArgs e)
        {
            OpenNI.Shutdown();
        }

        private void FrmMainLoad(object sender, EventArgs e)
        {
            HandleError(OpenNI.Initialize());
            OpenNI.OnDeviceConnected += OpenNiOnDeviceConnectionStateChanged;
            OpenNI.OnDeviceDisconnected += OpenNiOnDeviceConnectionStateChanged;
            UpdateDevicesList();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            // Create an instance of the open file dialog box.
            var openFileDialog1 = new OpenFileDialog
            {
                Filter = @"ONI files (*.oni)|*.oni",
                FilterIndex = 1,
                Multiselect = false,
                CheckFileExists = false,
                InitialDirectory = _lastDir,
                RestoreDirectory = true,
                FileName = Path.GetFileName(_oniName)
            };

            if (openFileDialog1.ShowDialog() != DialogResult.OK)
                return;

            _oniName = openFileDialog1.FileName;
            edFilePath.Text = _oniName;
            _lastDir = Path.GetDirectoryName(_oniName);
        }

        private void edFilePath_TextChanged(object sender, EventArgs e)
        {
            _oniName = edFilePath.Text;
        }

        #endregion
    }
}